local M = {}

M.custom = {
  n = {
    ["<F7>"] = {
      function()
        require("custom.plugins.configs.pomodoro").start()
      end,
      "Pomodoro start",
    },

    ["<F8>"] = {
      function()
        vim.lsp.buf.format({async = true})
      end,
      " Formating",
    },
  },
}

M.lazy_git = {
  n = {
    ["<leader>gg"] = {":LazyGit<CR>", "LazyGit"},
  }
}

M.telekasten = {
  n = {
    ["<leader>kf"] = {
      function()
        require("telekasten").find_notes()
      end,
      "Telekasten find_notes",
    },
    ["<leader>kg"] = {
      function()
        require("telekasten").search_notes()
      end,
      "Telekasten search_notes",
    },
    ["<leader>kd"] = {
      function()
        require("telekasten").goto_today()
      end,
      "Telekasten goto_today",
    },
    ["<leader>kl"] = {
      function()
        require("telekasten").follow_link()
      end,
      "Telekasten follow_link",
    },
    ["<leader>kn"] = {
      function()
        require("telekasten").new_note()
      end,
      "Telekasten new_note",
    },
    ["<leader>knc"] = {
      function()
        require("custom.plugins.configs.obsidian").new_call()
      end,
      "Telekasten new_call",
    },
    ["<leader>kc"] = {
      function()
        require("telekasten").show_calendar()
      end,
      "Telekasten show_calendar",
    },
    ["<leader>kb"] = {
      function()
        require("telekasten").show_backlinks()
      end,
      "Telekasten show_backlinks",
    },
    ["<leader>ky"] = {
      function()
        require("telekasten").yank_notelink()
      end,
      "Telekasten yank_notelink",
    },
    ["<leader>kt"] = {
      function()
        require("telekasten").toggle_todo()
      end,
      "Telekasten toggle_todo",
    },
  },
  -- i = {
  --   ["<leader>kk"] = {
  --     function()
  --       require("telekasten").insert_link()
  --     end,
  --     "Telekasten insert_link",
  --   },
  -- },
}

M.truezen = {
  n = {
    ["<leader>zf"] = {
      function()
        require("true-zen").focus()
      end,
      "True-Zen focus",
    },
    ["<leader>zm"] = {
      function()
        require("true-zen").minimalist()
      end,
      "True-Zen minimalist",
    },
    ["<leader>za"] = {
      function()
        require("true-zen").ataraxis()
      end,
      "True-Zen ataraxis",
    },
    ["<leader>zn"] = {":TZNarrow<CR>", "True-Zen narrow"},
    ["<leader>zb"] = {
      function()
        local ok, context_start, context_end = require("indent_blankline.utils").get_current_context(
          vim.g.indent_blankline_context_patterns,
          vim.g.indent_blankline_use_treesitter_scope
        )

        if ok then
          require("true-zen").narrow(context_start, context_end)
        end
      end,
      "True-Zen narrow codeblock"
    },
  },
  v = {
    ["<leader>zn"] = {":'<,'>TZNarrow<CR>", "True-Zen narrow"},
  }
}

M.lspconfig = {
  n = {
    ["<leader>re"] = {
      function ()
        vim.diagnostic.open_float()
      end,
      "   lsp line diagnostics",
    },

    ["<F4>"] = {
      function()
        require("nvchad_ui.renamer").open()
      end,
      "   lsp rename",
    },

    ["<F5>"] = {
      function()
        vim.lsp.buf.references()
      end,
      "   lsp references",
    },

    ["<F6>"] = {
      function()
        vim.lsp.buf.definition()
      end,
      "   lsp definition",
    },

    ["<leader>wa"] = {
      function()
        vim.lsp.buf.add_workspace_folder()
      end,
      "   add workspace folder",
    },

    ["<leader>wr"] = {
      function()
        vim.lsp.buf.remove_workspace_folder()
      end,
      "   remove workspace folder",
    },

    ["<leader>wl"] = {
      function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
      end,
      "   list workspace folders",
    },
  },
}

return M
