local telekasten_present, telekasten = pcall(require, "telekasten")

if not telekasten_present then
   return
end

local home = vim.fn.expand("~/vault")
local daily_home = home .. "/daily"
local calls_home = home .. "/calls"
local templates_home = home .. "/meta/templates"

local function setup()
  telekasten.setup({
    home = home,
    take_over_my_home = true,
    auto_set_file_type = true,
    dailies = daily_home,
    weeklies = daily_home,
    templates = templates_home,
    image_subdir = nil,
    extension = ".md",
    new_note_filename = "uuid-title",
    uuid_type = "%Y-%m-%d",
    uuid_sep = "|",
    follow_creates_nonexisting = true,
    dailies_create_nonexisting = true,
    weeklies_create_nonexisting = true,
    journal_auto_open = false,
    template_new_note = templates_home .. "/note template.md",
    template_new_daily = templates_home .. "/daily template.md",
    template_new_weekly = templates_home .. "/weekly template.md",
    image_link_style = "wiki",
    sort = "filename",
    plug_into_calendar = true,
    calendar_opts = {
      weeknm = 1,
      calendar_monday = 1,
      calendar_mark = "left-fit",
    },
    subdirs_in_links = false,
    close_after_yanking = true,
    insert_after_inserting = true,
    tag_notation = "#tag",
    command_palette_theme = "ivy",
    show_tags_theme = "ivy",
    template_handling = "prefer_new_note",
    new_note_location = "prefer_home",
    rename_update_links = "true",
    media_previewer = "telescope-media-files",
  })
end

local function created_note(filepath)
  require("custom.ui.popup").choices_menu(
    { "Edit call data", "Close" },
    "Call created",
    function(action)
      if not action then return end
      if action == "Edit call data" then
        vim.cmd("e " .. filepath)
      end
    end
  )
end


local function new_call()
  local today = os.date("%Y-%m-%d")
  require("custom.ui.popup").input(
    today .. "| ",
    "New call",
    function(title)
      if not title then return end
      local filepath = calls_home .. "/" .. title .. ".md"
      vim.cmd("silent !touch \"" .. filepath .. "\"")
      created_note(filepath)
    end,
    30
  )
end

local M = {
  setup = setup,
  new_call = new_call,
}

return M
