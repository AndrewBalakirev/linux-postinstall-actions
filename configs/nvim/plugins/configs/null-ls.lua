local null_ls = require "null-ls"

local formatting = null_ls.builtins.formatting
local lint = null_ls.builtins.diagnostics


local sources = {
  formatting.black,
  formatting.isort,
  lint.codespell,
  lint.misspell,
}

null_ls.setup {
   debug = false,
   sources = sources,
}

