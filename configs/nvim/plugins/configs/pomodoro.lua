local Pomodoro = {
  timer = nil,
  state = "stopped",
  work_started_at = 0,
  break_started_at = 0,
  timers_completed = 0,
  time_work = 25,
  time_break_short = 5,
  time_break_long = 20,
  timers_to_long_break = 4,
}

Pomodoro.time_break = function()
  local timebreak

  if Pomodoro.timers_completed == 0 then
    timebreak = Pomodoro.time_break_long
  else
    timebreak = Pomodoro.time_break_short
  end

  return timebreak
end

Pomodoro.time_remaining = function(duration, start)
  local time_remaining
  local seconds = duration * 60 - os.difftime(os.time(), start)

  if math.floor(seconds / 60) >= 60 then
    time_remaining = os.date("!%0H:%0M:%0S", seconds)
  else
    time_remaining = os.date("!%0M:%0S", seconds)
  end

  return time_remaining
end

Pomodoro.popup_callback = function(answer)
  if answer == "Take break" then
    Pomodoro.start_break()
  elseif answer == "Continue" then
    Pomodoro.start()
  elseif answer == "Stop" or answer == nil then
    Pomodoro.stop()
  end
end

Pomodoro.complete_menu_open = function()
  require("custom.ui.popup").choices_menu({ "Take break", "Stop" }, "Pomodoro", Pomodoro.popup_callback)
end

Pomodoro.break_complete_menu_open = function()
  require("custom.ui.popup").choices_menu({ "Continue", "Stop" }, "Pomodoro", Pomodoro.popup_callback)
end

Pomodoro.statusline = function()
  local status

  if Pomodoro.state == "stopped" then
      status = "ﮫ (inactive)"
  elseif Pomodoro.state == "started" then
      status = "羽" .. Pomodoro.time_remaining(Pomodoro.time_work, Pomodoro.work_started_at)
  else
      local break_minutes = Pomodoro.time_break()
      status = "ﲊ " .. Pomodoro.time_remaining(break_minutes, Pomodoro.break_started_at)
  end

  return status
end

Pomodoro.start_break = function()
  if Pomodoro.state == "started" then
    Pomodoro.timers_completed = (Pomodoro.timers_completed + 1) % Pomodoro.timers_to_long_break
    local break_milliseconds = Pomodoro.time_break() * 60 * 1000
    Pomodoro.timer:start(break_milliseconds, 0, vim.schedule_wrap(Pomodoro.break_complete_menu_open))
    Pomodoro.break_started_at = os.time()
    Pomodoro.state = "break"
  end
end

Pomodoro.start = function()
  if Pomodoro.state ~= "started" then
    Pomodoro.timer = vim.loop.new_timer()
    local work_milliseconds = Pomodoro.time_work * 60 * 1000

    Pomodoro.timer:start(work_milliseconds, 0, vim.schedule_wrap(Pomodoro.complete_menu_open))
    Pomodoro.work_started_at = os.time()
    Pomodoro.state = "started"
  end
end

Pomodoro.stop = function()
  if Pomodoro.state ~= "stopped" then
    Pomodoro.timer:stop()
    Pomodoro.timer:close()
    Pomodoro.state = "stopped"
  end
end


PomodoroInterface = {
  start = Pomodoro.start,
  stop = Pomodoro.stop,
  statusline = Pomodoro.statusline,
  status = function()
    print(Pomodoro.statusline())
  end,
}

return PomodoroInterface
