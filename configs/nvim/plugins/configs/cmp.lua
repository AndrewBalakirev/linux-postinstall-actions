local M = {}

local source_mapping = {
	luasnip = "[SNIP]",
	cmp_tabnine = "[TN]",
	buffer = "[Buffer]",
	nvim_lsp = "[LSP]",
	nvim_lua = "[Lua]",
	path = "[Path]",
}

local tabnine_kinds = {
  "Ну может так? А?",
  "Кхм! Попробуй так",
  "Господин, вот так",
  "А так если попробовать?",
  "Смотри че нашел!",
  "Ля! Че творится-то!",
  "У меня есть план",
  "Может сработать!",
  "Можно попробовать так",
}

M.sources = {
  { name = "cmp_tabnine" },
  { name = "luasnip" },
  { name = "nvim_lsp" },
  { name = "buffer" },
  { name = "nvim_lua" },
  { name = "path" },
}

M.formatting = {
  format = function(entry, vim_item)
    local icons = require("nvchad_ui.icons").lspkind
    local kind = vim_item.kind
    local menu = source_mapping[entry.source.name]
    local kind_icon = icons[kind]
    if entry.source.name == "cmp_tabnine" then
      kind_icon = ""
      kind = " " ..  tabnine_kinds[math.random(#tabnine_kinds)]
    end
    vim_item.kind = string.format("%s %s", kind_icon, kind)
    vim_item.menu = menu
    return vim_item
  end
}

return M
