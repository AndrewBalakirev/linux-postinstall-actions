local truezen_present, truezen = pcall(require, "true-zen")

if not truezen_present then
   return
end

truezen.setup({
  modes = {
    narrow = {
      run_ataraxis = false,
    },
    minimalist = {
      options = {
        number = true,
        showtabline = 1,
        colorcolumn = nil,
      },
    },
    ataraxis = {
			minimum_writing_area = {
				width = 126,
				height = 70,
			},
      callbacks = {
				open_pre = function ()
				  vim.opt.colorcolumn = nil
				end,
				close_pos = function ()
				  vim.opt.colorcolumn = "120"
				end,
			},
    },
  },
})
