local tabnine_present, tabnine = pcall(require, "cmp_tabnine.config")

if not tabnine_present then
   return
end

tabnine:setup({
  max_lines = 1000;
  max_num_results = 20;
  sort = true;
  run_on_every_keystroke = true;
  snippet_placeholder = "--..--";
  ignored_file_types = {
    -- lua = true
  };
  show_prediction_strength = false;
})
