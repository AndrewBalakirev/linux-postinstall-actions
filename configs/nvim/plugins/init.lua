local M = {
  -- install new plugins --
  {
    "iamcco/markdown-preview.nvim",
    run = "cd app && npm install",
    setup = function()
      vim.g.mkdp_filetypes = {"markdown"}
    end,
    ft = {"markdown"},
  },
  -- ["renerocksai/telekasten.nvim"] = {
  --   requires = {
  --     "renerocksai/calendar-vim",
  --     "nvim-telescope/telescope-media-files.nvim",
  --   },
  --   config = function()
  --     require("packer").loader("telescope.nvim")
  --     require("custom.plugins.configs.obsidian").setup()
  --   end,
  -- },
  {
    "kdheepak/lazygit.nvim",
    event = "VimEnter",
  },
  {
    "wakatime/vim-wakatime",
    event = "VimEnter",
  },
  {
    "mattn/emmet-vim",
    event = "VimEnter",
  },
  {
    "Pocco81/true-zen.nvim",
    config = function()
      require("custom.plugins.configs.true-zen")
    end
  },
  {
    "tzachar/cmp-tabnine",
    after = "nvim-cmp",
    build = "./install.sh",
    event = "VimEnter",
    config = function()
      require("custom.plugins.configs.tabnine")
    end
  },

  -- override nvchad plugins --
  {
    "hrsh7th/nvim-cmp",
    config = require("custom.plugins.configs.cmp"),
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      "jose-elias-alvarez/null-ls.nvim",
      config = function()
        require "custom.plugins.configs.null-ls"
      end,
    },
    config = function()
      require("plugins.configs.lspconfig")
      require("custom.plugins.configs.lspconfig")
    end,
  },
  {
    "williamboman/mason.nvim",
    override_options = {
      ensure_installed = {
        "lua-language-server",
        "pyright",
        "black",
        "debugpy",
      },
    },
  },
  {
    "folke/which-key.nvim",
    disable = false,
  },

  {
    "nvim-tree/nvim-tree.lua",
    opts = function()
      return require "custom.plugins.configs.nvim-tree"
    end,
  },

}

return M
