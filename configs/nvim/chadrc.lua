local M = {}

M.ui = require("custom.ui.chad_ui")
M.mappings = require("custom.mappings")
M.plugins = "custom.plugins"

return M
