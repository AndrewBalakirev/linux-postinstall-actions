local M = {}

M.theme = "catppuccin"
M.theme_toggle = { "catppuccin", "catppuccin" }
M.hl_override = {
  Comment = {fg = "#f1af1a"},
  DiagnosticError = {fg = "white"},
  DiagnosticWarn = {fg = "white"},
  DiagnosticInfo = {fg = "white"},
  DiagnosticHint = {fg = "white"},
}
M.statusline = {
  overriden_modules = function()
    return require("custom.ui.statusline")
  end
}
M.tabufline = {
  lazyload = false,
  overriden_modules = function()
    return require("custom.ui.tabufline")
  end
}

return M
