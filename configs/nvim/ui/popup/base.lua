local get_popup_width = function(what)
  local width = 1
  if type(what) == "table" then
    for value in pairs(what) do
      local value_width = tostring(value):len()
      if value_width > width then
        width = value_width
      end
    end
  else
    width = tostring(what):len()
  end
  return width
end

local get_popup_height = function(what)
  local height = 1
  if type(what) == "table" then
    height = #(what)
  end
  return height
end

local register_map = function(win_id, mode, key, action)
  local map_opts = { noremap = true, silent = true }
  local command = "<cmd>lua require('custom.ui.popup.callbacks')." .. action .. "(" .. win_id .. ")<CR>"
  vim.api.nvim_buf_set_keymap(0, mode, key, command, map_opts)
end

local register_maps = function(win_id, mappings)
  for mode, v in pairs(mappings) do
    for _, map in pairs(v) do
      register_map(win_id, mode, map.key, map.action)
    end
  end
end

local popup = {}

popup._callbacks = {}

popup.open = function(what, title, callback, params)
  local height, width, mappings = params.height, params.width, params.mappings

  width = width or math.max(get_popup_width(what), title:len() + 2)
  height = height or get_popup_height(what)

  local win_id = require("plenary.popup").create(what, {
    title = title,
    borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
    borderhighlight = "RenamerBorder",
    titlehighlight = "RenamerTitle",
    focusable = true,
    height = height,
    width = width,
  })
  popup._callbacks[win_id] = callback
  register_maps(win_id, mappings or {})
  return win_id
end

return popup
