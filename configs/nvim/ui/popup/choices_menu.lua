return function(what, title, callback)
  local win_id = require("custom.ui.popup.base").open(
    what, title, callback,
    {
      mappings = {
        n = {
          {key = "q", action = "close"},
          {key = "<ESC>", action = "close"},
          {key = "<space>", action = "apply_line"},
          {key = "<CR>", action = "apply_line"},
        }
      }
    }
  )
  vim.cmd("stopinsert")
  return win_id
end
