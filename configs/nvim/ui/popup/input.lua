return function(what, title, callback, width)
  local win_id = require("custom.ui.popup.base").open(
    what, title, callback,
    {
      mappings = {
        n = {
          {key = "<Esc>", action = "close"},
          {key = "<CR>", action = "apply_line"},
        },
        i = {
          {key = "<CR>", action = "apply_line"},
        },
      },
      width = width,
    }
  )
  vim.cmd("normal $")
  vim.cmd("startinsert")
  return win_id
end
