return {
  choices_menu = require("custom.ui.popup.choices_menu"),
  input = require("custom.ui.popup.input"),
  textarea = require("custom.ui.popup.textarea"),
  create = require("custom.ui.popup.base").open,
}
