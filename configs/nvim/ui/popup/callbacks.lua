local call_callback_and_close_popup = function(win_id, value)
  local popup = require("custom.ui.popup.base")
  vim.api.nvim_win_close(win_id, true)
  popup._callbacks[win_id](value)
  popup._callbacks[win_id] = nil
end

local callbacks = {}

callbacks.apply_line = function(win_id)
  local option = vim.trim(vim.fn.getline ".")
  call_callback_and_close_popup(win_id, option)
end

callbacks.apply_lines = function(win_id)
  local lines = vim.api.nvim_buf_get_lines(0, 0, -1, false)
  local text = table.concat(lines, "\n")
  call_callback_and_close_popup(win_id, text)
end

callbacks.close = function(win_id)
  call_callback_and_close_popup(win_id)
end

return callbacks
