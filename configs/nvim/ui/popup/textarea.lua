return function(what, title, callback, height, width)
  local win_id = require("custom.ui.popup.base").open(
    what, title, callback,
    {
      mappings = {
        n = {
          {key = "<Esc>", action = "close"},
          {key = "<CR>", action = "apply_lines"},
        }
      },
      height = height,
      width = width,
    }
  )
  vim.cmd("startinsert")
  return win_id
end
