local default_sep_icons = {
  default = { left = "", right = " " },
  round = { left = "", right = "" },
  block = { left = "█", right = "█" },
  arrow = { left = "", right = "" },
}

local sep_l = default_sep_icons["block"]["left"]

local M = {}

M.cursor_position = function()
  local left_sep = "%#St_pos_sep#" .. sep_l .. "%#St_pos_icon#" .. " "

  local row , column = unpack(vim.api.nvim_win_get_cursor(0))
  row = "%4(" .. row .. "%)"
  column = "%-4(" .. column .. "%)"
  local text = row .. ":" .. column

  return left_sep .. "%#St_pos_text#" .. text
end

return M
