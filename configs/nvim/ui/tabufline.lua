local fn = vim.fn
local env = vim.env
local tabufline_modules = require("nvchad_ui.tabufline.modules")

local getNvimTreeWidth = function()
  for _, win in pairs(vim.api.nvim_tabpage_list_wins(0)) do
    if vim.bo[vim.api.nvim_win_get_buf(win)].ft == "NvimTree" then
      return vim.api.nvim_win_get_width(win) + 1
    end
  end
  return 0
end

local virtualenv = function()
  if not env.VIRTUAL_ENV then return "" end

  local venv_name = fn.fnamemodify(env.VIRTUAL_ENV, ":t") .. " "
  return (vim.o.columns > 120 and "%#St_ReplaceMode#" .. " venv: " .. venv_name) or ""
end

local M = {}

M.buttons = function()
  return virtualenv() .. tabufline_modules.buttons()
end

M.CoverNvimTree = function()
  local pomodoro = require("custom.plugins.configs.pomodoro").statusline()
  local gap_width = (getNvimTreeWidth() - pomodoro:len()) / 2 + 1
  local space_gap = string.rep(" ", gap_width)
  return "%#NvimTreeNormal#" .. space_gap .. pomodoro .. space_gap
end

return M
