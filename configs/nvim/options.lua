local autocmd = vim.api.nvim_create_autocmd
local opt = vim.opt
local g = vim.g

g.loaded_python3_provider = true

-- настройки мыши
opt.mouse = "a"
opt.mousemodel = "extend"

-- Отключаем своп файлы
opt.swapfile = false

-- Переносим на другую строчку, разрываем строки
opt.wrap = true
opt.linebreak = true
opt.colorcolumn = "120"

-- Кодировка файлов по умолчанию
opt.encoding = "utf-8"

-- Включаем нумерацию строк
opt.nu = true

-- Настройки табов default
opt.tabstop = 4
opt.shiftwidth = 4
opt.softtabstop = 4 -- 4 пробела в табе
opt.expandtab = true -- Ставим табы пробелами
opt.smarttab = true

-- Настройки табов в зависимости от типа файла
local tabs_by_filetype = function(filetype, tablength)
  autocmd("FileType", {
    pattern = filetype,
    callback = function ()
      opt.shiftwidth = tablength
      opt.softtabstop = tablength
      opt.tabstop = tablength
    end
  })
end

tabs_by_filetype("python", 4)
tabs_by_filetype("vue", 2)
tabs_by_filetype("css", 2)
tabs_by_filetype("scss", 2)
tabs_by_filetype("html", 4)
tabs_by_filetype("htmldjango", 4)
tabs_by_filetype("yaml", 2)
tabs_by_filetype("yml", 2)
tabs_by_filetype("lua", 2)
tabs_by_filetype("javascript", 2)
