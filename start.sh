echo '**************************'
echo '*  Install dependencies  *'
echo '**************************'

sudo apt-get update && sudo apt-get install -y \
    gcc \
    cmake \
    python3 \
    python3-dev \
    python3-pip \
    python3-venv \
    snap \
    snapd \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

pip3 install \
    poetry \
    flake8 \
    flake8-commas \
    flake8-cognitive-complexity \
    flake8-class-attributes-order \
    isort \
    mccabe

sudo snap install telegram-desktop


echo '**************************'
echo '*     Install docker     *'
echo '**************************'

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin


echo '**************************'
echo '*     Install neovim     *'
echo '**************************'

wget https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.deb
sudo dpkg -i ./nvim-linux64.deb
rm -fr ./nvim-linux64.deb
git clone https://github.com/NvChad/NvChad ~/.config/nvim --depth 1


echo '**************************'
echo '*      Copy configs      *'
echo '**************************'

script_path=`realpath $0`
script_dir=`dirname $script_path`

cp -f $script_dir/configs/bash_aliases ~/.bash_aliases
cp -f $script_dir/configs/flake8 ~/.config/flake8
cp -f $script_dir/configs/isort.cfg ~/.config/.isort.cfg
cp -fr $script_dir/configs/nvim ~/.config/nvim/lua/custom
